package com.godohdev.githubsearch.external.extention

import android.text.TextUtils
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.godohdev.githubsearch.R

/**
 *
 * Created by Wahyu Permadi on 04/04/20.
 * Android Engineer
 *
 **/

class BindingExtension{
    companion object{

        @JvmStatic
        @BindingAdapter("android:src")
        fun setImageUrl(view: ImageView, url: String) {
            if (url.isEmpty()){
                view.setImageDrawable(ContextCompat.getDrawable(view.context, R.drawable.ic_launcher_foreground))
            }else{
                Glide.with(view.context)
                    .asBitmap()
                    .centerCrop()
                    .load(url)
                    .into(view)
            }
        }
    }
}