package com.godohdev.githubsearch

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.godohdev.githubsearch.di.DaggerAppComponent
import com.godohdev.githubsearch.di.module.DataModule
import com.godohdev.githubsearch.di.module.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

/**
 *
 * Created by Wahyu Permadi on 04/04/20.
 * Android Engineer
 *
 **/

class MainApp : Application(), HasAndroidInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .provideApp(this)
            .provideDataModule(DataModule(this))
            .provideNetworkModule(NetworkModule(this))
            .build()
            .inject(this)
    }
}