package com.godohdev.githubsearch.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.godohdev.githubsearch.R
import com.godohdev.githubsearch.data.entity.LoadStatus
import com.godohdev.githubsearch.data.entity.Loading
import com.godohdev.githubsearch.data.entity.Success
import com.godohdev.githubsearch.data.entity.User
import com.godohdev.githubsearch.databinding.ItemUserBinding
import kotlinx.android.synthetic.main.layout_shimmer.view.layoutShimmer

/**
 *
 * Created by Wahyu Permadi on 04/04/20.
 * Android Engineer
 *
 **/

class MainAdapter : PagedListAdapter<User, RecyclerView.ViewHolder>(DIFF_CALLBACK){
    private var loadStatus: LoadStatus? = null
    private var oldItemCount = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM_TYPE){
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemUserBinding.inflate(inflater, parent, false)
            ItemViewHolder(binding)
        }else{
            val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_shimmer, parent, false)
            LoadViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        oldItemCount = super.getItemCount()
        if (getItemViewType(position) == ITEM_TYPE){
            val user = getItem(position)
            holder.apply {
                user?.let {data ->
                    (holder as ItemViewHolder).bind(data)
                }
            }
        }
    }

    fun updateLoadStatus(loadStatus: LoadStatus) {
        this.loadStatus = loadStatus
        notifyItemChanged(super.getItemCount())
    }

    class ItemViewHolder(private val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: User) {
            binding.apply {
                user = data
                executePendingBindings()
            }
        }
    }

    class LoadViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun getItemCount(): Int {
        return super.getItemCount() + if(showLoadStatus()) 1 else 0
    }

    override fun getItemViewType(position: Int): Int = if (showLoadStatus() && position == itemCount - 1) LOAD_TYPE else ITEM_TYPE

    private fun showLoadStatus() = loadStatus != null && loadStatus is Loading

    companion object{
        private const val ITEM_TYPE = 1
        private const val LOAD_TYPE = 0

        private val DIFF_CALLBACK = object: DiffUtil.ItemCallback<User>(){
            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem == newItem
            }
        }
    }
}