package com.godohdev.githubsearch.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.annotation.NonNull
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.SearchView.OnQueryTextListener
import androidx.core.util.Consumer
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.godohdev.githubsearch.R
import com.godohdev.githubsearch.data.entity.Error
import com.godohdev.githubsearch.data.entity.Loading
import com.godohdev.githubsearch.data.entity.Success
import com.godohdev.githubsearch.di.viewmodel.ViewModelFactory
import com.godohdev.githubsearch.external.extention.hide
import com.godohdev.githubsearch.external.extention.setDivider
import com.godohdev.githubsearch.external.extention.showSnackbar
import com.godohdev.githubsearch.external.extention.visible
import com.godohdev.githubsearch.presentation.base.BaseActivity
import com.godohdev.githubsearch.presentation.base.BaseViewModel
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding3.appcompat.queryTextChangeEvents
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.shimmerContainer
import kotlinx.android.synthetic.main.activity_main.userRecyclerView
import org.jetbrains.anko.toast
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var mainAdapter: MainAdapter
    private lateinit var viewModel: MainViewModel
    override fun setupLayoutId(): Int  = R.layout.activity_main

    override fun getViewModel(): BaseViewModel = viewModel

    override fun onActivityReady(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]
        initUi()
        initData()
        fetchNewUser("wahyupermadie")
    }

    private fun fetchNewUser(user: String) {
        viewModel.getGithubUser(user)
    }

    @SuppressLint("CheckResult")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val menuItem = menu?.findItem(R.id.action_search)
        val searchView = menuItem?.actionView as SearchView
        searchView.queryTextChangeEvents()
            .debounce(500, TimeUnit.MILLISECONDS) // stream will go down after 1 second inactivity of user
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                if (it.queryText.isNotEmpty()){
                    fetchNewUser(it.queryText.toString())
                }
            }
        return true
    }
    private fun initData() {
        viewModel.githubDataSource.observe(this, Observer {})
        viewModel.userList.observe(this, Observer {
            mainAdapter.submitList(it)
        })
        viewModel.loadStatus?.observe(this, Observer {
            when(it){
                is Error -> {
                    showSnackbar(it.message, Snackbar.LENGTH_LONG)
                }
            }
            mainAdapter.updateLoadStatus(it)
        })
    }

    private fun initUi() {
        mainAdapter = MainAdapter()
        userRecyclerView.apply {
            adapter = mainAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
            setDivider(R.drawable.rv_diver)
        }
    }

    override fun showLoading() {
        super.showLoading()
        shimmerContainer.visible()
        userRecyclerView.hide()
    }

    override fun hideLoading() {
        super.hideLoading()
        shimmerContainer.hide()
        userRecyclerView.visible()
    }

}
