package com.godohdev.githubsearch.presentation.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.godohdev.githubsearch.data.entity.Error
import com.godohdev.githubsearch.data.entity.Loading
import com.godohdev.githubsearch.data.entity.Success
import com.godohdev.githubsearch.external.extention.showSnackbar
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

abstract class BaseActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    abstract fun setupLayoutId() : Int
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(setupLayoutId())
        onActivityReady(savedInstanceState)
        setupObserver(getViewModel())
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector
    }

    abstract fun getViewModel() : BaseViewModel

    abstract fun onActivityReady(savedInstanceState: Bundle?)

    private fun setupObserver(baseViewModel: BaseViewModel) {
        baseViewModel.initialStatus?.observe(this, Observer {
            when(it){
                is Loading -> {
                    showLoading()
                }
                is Success -> {
                    hideLoading()
                }
                is Error -> {
                    hideLoading()
                    showSnackbar(it.message, Snackbar.LENGTH_LONG)
                }
            }
        })
    }

    protected open fun hideLoading(){

    }

    protected open fun showLoading(){

    }
}