package com.godohdev.githubsearch.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import com.godohdev.githubsearch.data.domain.UserUseCase
import com.godohdev.githubsearch.data.entity.Error
import com.godohdev.githubsearch.data.entity.LoadStatus
import com.godohdev.githubsearch.data.entity.Loading
import com.godohdev.githubsearch.data.entity.Success
import com.godohdev.githubsearch.data.entity.User
import com.godohdev.githubsearch.data.entity.UserListingModel
import com.godohdev.githubsearch.data.local.LocalDataSource
import com.godohdev.githubsearch.external.Event
import com.godohdev.githubsearch.external.coroutine.CoroutineContextProvider
import com.godohdev.githubsearch.presentation.base.BaseViewModel
import kotlinx.coroutines.launch

/**
 *
 * Created by Wahyu Permadi on 04/04/20.
 * Android Engineer
 *
 **/

class MainViewModel (
    private val useCase: UserUseCase
) : BaseViewModel(){

    private var _githubDataSource = MediatorLiveData<UserListingModel>()
    val githubDataSource : LiveData<UserListingModel>
        get() = _githubDataSource

    private var _userList= Transformations.switchMap(githubDataSource) {
            it.pagedList
        }
    val userList : LiveData<PagedList<User>>
        get() = _userList

    override var _loadStatus: LiveData<LoadStatus> = Transformations.switchMap(githubDataSource) {
        it.loadStatus
    }

    override var _initialStatus = Transformations.switchMap(githubDataSource) {
        it.refreshStatus
    }

    fun getGithubUser(key: String) {
        _githubDataSource.addSource(useCase.invoke(key)){
            _githubDataSource.value = it
        }
    }
}