package com.godohdev.githubsearch.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.godohdev.githubsearch.data.entity.LoadStatus
import com.godohdev.githubsearch.external.Event

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

abstract class BaseViewModel : ViewModel() {

    // FOR ERROR HANDLER
    protected val _errorHandler = MutableLiveData<Event<String>>()
    val errorHandler: LiveData<Event<String>> get() = _errorHandler

    protected val _loadingHandler = MutableLiveData<Boolean>(false)
    val loadingHandler: LiveData<Boolean> get() = _loadingHandler

    protected open val _loadStatus : LiveData<LoadStatus>? = null
    val loadStatus : LiveData<LoadStatus>? get() = _loadStatus

    protected open val _initialStatus : LiveData<LoadStatus>? = null
    val initialStatus : LiveData<LoadStatus>? get() = _initialStatus

}