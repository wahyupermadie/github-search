package com.godohdev.githubsearch.di.scope

import javax.inject.Scope

/**
 *
 * Created by Wahyu Permadi on 04/04/20.
 * Android Engineer
 *
 **/

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope