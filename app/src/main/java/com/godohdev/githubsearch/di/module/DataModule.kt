package com.godohdev.githubsearch.di.module

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.godohdev.githubsearch.data.domain.UserUseCase
import com.godohdev.githubsearch.data.domain.UserUseCaseImpl
import com.godohdev.githubsearch.data.local.LocalDataSource
import com.godohdev.githubsearch.data.local.LocalDataSourceImpl
import com.godohdev.githubsearch.data.local.dao.UserDao
import com.godohdev.githubsearch.data.local.db.GithubDatabase
import com.godohdev.githubsearch.data.network.NetworkService
import com.godohdev.githubsearch.data.repository.UserRepository
import com.godohdev.githubsearch.data.repository.UserRepositoryImpl
import com.godohdev.githubsearch.external.coroutine.AppCoroutineContextProvider
import com.godohdev.githubsearch.external.coroutine.CoroutineContextProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

@Module
class DataModule (private val context: Context){

    @Singleton
    @Provides
    fun providesGithubDatabase() : GithubDatabase =
        Room.databaseBuilder(context,
            GithubDatabase::class.java,
            "db_github"
        ).fallbackToDestructiveMigration().build()

    @Singleton
    @Provides
    fun provideUserDao(githubDatabase: GithubDatabase) : UserDao = githubDatabase.userDao()

    @Singleton
    @Provides
    fun provideLocalDataSource(userDao: UserDao) : LocalDataSource = LocalDataSourceImpl(userDao)

    @Singleton
    @Provides
    fun provideUserRepository(
        localDataSource: LocalDataSource,
        networkService: NetworkService
    ) : UserRepository = UserRepositoryImpl(networkService, localDataSource)

    @Singleton
    @Provides
    fun provideUserUseCase(
        userRepository: UserRepository
    ) : UserUseCase = UserUseCaseImpl(userRepository)

    @Singleton
    @Provides
    fun provideCoroutineContext() : CoroutineContextProvider = AppCoroutineContextProvider()
}