package com.godohdev.githubsearch.di.module

import androidx.lifecycle.ViewModel
import com.godohdev.githubsearch.data.domain.UserUseCase
import com.godohdev.githubsearch.data.local.LocalDataSource
import com.godohdev.githubsearch.di.viewmodel.ViewModelFactory
import com.godohdev.githubsearch.di.viewmodel.ViewModelKey
import com.godohdev.githubsearch.external.coroutine.AppCoroutineContextProvider
import com.godohdev.githubsearch.external.coroutine.CoroutineContextProvider
import com.godohdev.githubsearch.presentation.MainViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Provider

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

@Module
class ViewModelModule {
    @Provides
    fun viewModelFactory(providerMap: MutableMap<Class<out ViewModel>, Provider<ViewModel>>): ViewModelFactory {
        return ViewModelFactory(providerMap)
    }

    @IntoMap
    @ViewModelKey(MainViewModel::class)
    @Provides
    fun provideMovieViewModel(
        useCase: UserUseCase
    ) : ViewModel = MainViewModel(
        useCase
    )
}