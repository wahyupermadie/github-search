package com.godohdev.githubsearch.di.module

import android.app.Application
import android.util.Log
import com.godohdev.githubsearch.BuildConfig
import com.godohdev.githubsearch.data.network.NetworkService
import com.godohdev.githubsearch.external.extention.isConnected
import com.readystatesoftware.chuck.ChuckInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.OkHttpClient.Builder
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

@Module
class NetworkModule(
    private val context : Application
) {

    @Provides
    @Singleton
    fun moshi() : Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val cacheSize = (5 * 1024 * 1024).toLong()
        val myCache = Cache(context.baseContext.cacheDir, cacheSize)
        return Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(ChuckInterceptor(context.applicationContext))
            .addInterceptor { chain ->
                val request = chain.request()
                if (!context.isConnected()){
                    request.
                    newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + 60)
                        .removeHeader("Pragma")
                        .build()
                }
                chain.proceed(request)
            }
            .addNetworkInterceptor { chain ->
                val request = chain.request()
                /*
                    get data from cache with max age is 2 hours, if expired, fetch automatically
                 */
                request
                    .newBuilder()
                    .header("Cache-Control", "public, max-age=" + 60 * 2)
                    .removeHeader("Pragma")
                    .build()
                chain.proceed(request)
            }
            .cache(myCache)
            .build()
    }

    @Provides
    @Singleton
    fun provideRestClient(okHttpClient: OkHttpClient, moshi: Moshi) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
    }

    @Provides
    @Singleton
    fun provideNetworkService(retrofit: Retrofit): NetworkService {
        return retrofit.create(NetworkService::class.java)
    }

}