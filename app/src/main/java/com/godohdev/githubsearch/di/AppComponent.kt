package com.godohdev.githubsearch.di

import android.app.Application
import com.godohdev.githubsearch.MainApp
import com.godohdev.githubsearch.di.builder.ActivityBuilder
import com.godohdev.githubsearch.di.module.DataModule
import com.godohdev.githubsearch.di.module.NetworkModule
import com.godohdev.githubsearch.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBuilder::class,
        DataModule::class,
        ViewModelModule::class,
        NetworkModule::class
    ]
)
@Singleton
interface AppComponent {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun provideApp(application: Application): Builder
        fun provideDataModule(dataModule: DataModule): Builder
        fun provideNetworkModule(networkModule: NetworkModule): Builder
        fun build() : AppComponent
    }
    fun inject(mainApp: MainApp)
}