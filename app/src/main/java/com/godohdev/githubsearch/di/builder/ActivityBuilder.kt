package com.godohdev.githubsearch.di.builder

import com.godohdev.githubsearch.di.scope.ActivityScope
import com.godohdev.githubsearch.presentation.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun bindMainActivity() : MainActivity
}