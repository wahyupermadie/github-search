package com.godohdev.githubsearch.data.repository

import androidx.lifecycle.LiveData
import com.godohdev.githubsearch.data.entity.UserListingModel

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

interface UserRepository {
    fun getGithubUser(userName: String) : LiveData<UserListingModel>
}