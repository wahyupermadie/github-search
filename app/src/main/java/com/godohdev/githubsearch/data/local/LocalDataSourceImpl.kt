package com.godohdev.githubsearch.data.local

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.godohdev.githubsearch.data.entity.User
import com.godohdev.githubsearch.data.local.dao.UserDao

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

class LocalDataSourceImpl (
    private val userDao: UserDao
) : LocalDataSource {

    override suspend fun insertUser(user: User): Long {
        return userDao.insertUser(user)
    }

    override suspend fun getUser(userName: String, page: Int): List<User> {
        return userDao.getUserList("%${userName}%", page)
    }
}