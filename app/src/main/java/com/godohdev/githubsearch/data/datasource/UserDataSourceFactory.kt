package com.godohdev.githubsearch.data.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.godohdev.githubsearch.data.entity.User
import com.godohdev.githubsearch.data.local.LocalDataSource
import com.godohdev.githubsearch.data.network.NetworkService
import kotlin.coroutines.CoroutineContext

/**
 *
 * Created by Wahyu Permadi on 04/04/20.
 * Android Engineer
 *
 **/

class UserDataSourceFactory(
    private val networkService: NetworkService,
    private val localDataSource: LocalDataSource,
    private val coroutineContext: CoroutineContext,
    private val userName: String
) : DataSource.Factory<Int, User>() {

    val dataSourceLiveData = MutableLiveData<UserDataSource>()

    override fun create(): DataSource<Int, User> {
        val userDataSource = UserDataSource(
            networkService,
            localDataSource,
            coroutineContext,
            userName
        )
        dataSourceLiveData.postValue(userDataSource)
        return userDataSource
    }
}