package com.godohdev.githubsearch.data.datasource

import android.accounts.NetworkErrorException
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.godohdev.githubsearch.BuildConfig
import com.godohdev.githubsearch.data.entity.Error
import com.godohdev.githubsearch.data.entity.LoadStatus
import com.godohdev.githubsearch.data.entity.Loading
import com.godohdev.githubsearch.data.entity.Success
import com.godohdev.githubsearch.data.entity.User
import com.godohdev.githubsearch.data.local.LocalDataSource
import com.godohdev.githubsearch.data.network.NetworkService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

/**
 *
 * Created by Wahyu Permadi on 04/04/20.
 * Android Engineer
 *
 **/

class UserDataSource (
    private val networkService: NetworkService,
    private val localDataSource: LocalDataSource,
    coroutineContext: CoroutineContext,
    private val userName: String
) : PageKeyedDataSource<Int, User>(){
    private val job = Job()
    private val scope = CoroutineScope(coroutineContext + job)
    private val supervisorJob = SupervisorJob()

    val loadStatus = MutableLiveData<LoadStatus>()
    val initStatus = MutableLiveData<LoadStatus>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, User>) {
        initStatus.postValue(Loading(""))
        scope.launch(supervisorJob) {
            try {
                val response =
                    networkService.getUsersGithub(
                        name= userName,
                        page = "1",
                        accessToken = BuildConfig.APP_KEY
                    )
                when{
                    response.isSuccessful -> {
                        response.body()?.let {
                            it.items?.forEach { user ->
                                user.let {
                                    it.page = 1
                                }
                                localDataSource.insertUser(user)
                            }
                        }

                        callback.onResult(localDataSource.getUser(userName, 1), null, 2)
                        initStatus.postValue(Success(200))
                    }
                }
            }catch (e: Exception){
                when(e){
                    is NetworkErrorException, is UnknownHostException -> {
                        initStatus.postValue(Error("Tidak ada koneksi internet"))
                    }
                    else -> initStatus.postValue(Error("Maaf, terjadi kesalahan pada server"))
                }
                callback.onResult(localDataSource.getUser(userName, 1), null, 2)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        loadStatus.postValue(Loading(""))
        scope.launch(supervisorJob) {
            try {
                val response =
                    networkService.getUsersGithub(
                        name= userName,
                        page = params.key.toString(),
                        accessToken = BuildConfig.APP_KEY
                    )
                when{
                    response.isSuccessful -> {
                        response.body()?.let {
                            it.items?.forEach { user ->
                                user.let {
                                    it.page = params.key
                                }
                                localDataSource.insertUser(user)
                            }
                        }

                        callback.onResult(localDataSource.getUser(userName, params.key), params.key + 1)
                        loadStatus.postValue(Success(200))
                    }
                }
            }catch (e: Exception){
                when(e){
                    is NetworkErrorException, is UnknownHostException -> {
                        loadStatus.postValue(Error("Tidak ada koneksi internet"))
                    }
                    else -> loadStatus.postValue(Error("Maaf, terjadi kesalahan pada server"))
                }
                callback.onResult(localDataSource.getUser(userName, params.key), params.key + 1)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        TODO("Not yet implemented")
    }

    override fun invalidate() {
        super.invalidate()
        job.cancel()
    }
}