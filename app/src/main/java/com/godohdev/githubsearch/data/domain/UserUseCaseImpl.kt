package com.godohdev.githubsearch.data.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.godohdev.githubsearch.data.entity.UserListingModel
import com.godohdev.githubsearch.data.repository.UserRepository

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

class UserUseCaseImpl (
    private val userRepository: UserRepository
): UserUseCase{
    override fun invoke(userName: String): LiveData<UserListingModel> {
        return Transformations.map(userRepository.getGithubUser(userName)){
            it
        }
    }
}