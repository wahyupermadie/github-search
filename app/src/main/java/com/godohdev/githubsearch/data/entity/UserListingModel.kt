package com.godohdev.githubsearch.data.entity

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

/**
 *
 * Created by Wahyu Permadi on 04/04/20.
 * Android Engineer
 *
 **/

data class UserListingModel(
    val pagedList: LiveData<PagedList<User>>,
    val loadStatus: LiveData<LoadStatus>,
    val refreshStatus: LiveData<LoadStatus>
)

sealed class LoadStatus
data class Success(val status: Int) : LoadStatus()
data class Loading(val content: String) : LoadStatus()
data class Error(val message: String) : LoadStatus()