package com.godohdev.githubsearch.data.domain

import androidx.lifecycle.LiveData
import com.godohdev.githubsearch.data.entity.UserListingModel

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

interface UserUseCase {
    operator fun invoke(userName: String) : LiveData<UserListingModel>
}