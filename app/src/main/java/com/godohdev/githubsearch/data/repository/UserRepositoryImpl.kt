package com.godohdev.githubsearch.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.godohdev.githubsearch.data.datasource.UserDataSourceFactory
import com.godohdev.githubsearch.data.entity.UserListingModel
import com.godohdev.githubsearch.data.local.LocalDataSource
import com.godohdev.githubsearch.data.network.NetworkService
import kotlinx.coroutines.Dispatchers

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

class UserRepositoryImpl (
    private val networkService: NetworkService,
    private val localDataSource: LocalDataSource
): UserRepository{
    private val resourceData = MutableLiveData<UserListingModel>()
    private val config = PagedList.Config.Builder()
        .setPageSize(5)
        .setInitialLoadSizeHint(10)
        .setEnablePlaceholders(false)
        .build()

    override fun getGithubUser(userName: String): LiveData<UserListingModel> {
        val userDataSourceFactory = UserDataSourceFactory(
            networkService,
            localDataSource,
            Dispatchers.IO,
            userName
        )

        val userPageList = LivePagedListBuilder(userDataSourceFactory, config).build()

        val loadStatus = Transformations.switchMap(userDataSourceFactory.dataSourceLiveData) {
            it.loadStatus
        }
        val initStatus = Transformations.switchMap(userDataSourceFactory.dataSourceLiveData) {
            it.initStatus
        }

        val userListingModel =  UserListingModel(
            pagedList = userPageList,
            loadStatus = loadStatus,
            refreshStatus = initStatus
        )

        resourceData.value = userListingModel
        return resourceData
    }
}