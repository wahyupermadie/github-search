package com.godohdev.githubsearch.data.network

import com.godohdev.githubsearch.data.entity.UserResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

interface NetworkService {
    @GET("users")
    suspend fun getUsersGithub(
        @Query("q") name : String,
        @Query("page") page : String,
        @Query("per_page") perPage : Int = 10,
        @Query("access_token") accessToken : String
    ) : Response<UserResponse>
}