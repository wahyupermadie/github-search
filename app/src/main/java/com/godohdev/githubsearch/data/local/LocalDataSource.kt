package com.godohdev.githubsearch.data.local

import com.godohdev.githubsearch.data.entity.User

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

interface LocalDataSource {
    suspend fun insertUser(user: User): Long
    suspend fun getUser(userName: String, page: Int): List<User>
}