package com.godohdev.githubsearch.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.godohdev.githubsearch.data.entity.User
import com.godohdev.githubsearch.data.local.dao.UserDao

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

@Database(entities = [User::class], exportSchema = false, version = 1)
abstract class GithubDatabase : RoomDatabase(){
    abstract fun userDao() : UserDao
}