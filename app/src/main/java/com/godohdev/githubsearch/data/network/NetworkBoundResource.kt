package com.godohdev.githubsearch.data.network

import android.accounts.NetworkErrorException
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.net.UnknownHostException
import kotlin.coroutines.coroutineContext

/**
 *
 * Created by Wahyu Permadi on 03/04/20.
 * Android Engineer
 *
 **/

abstract class NetworkBoundResource<ResultType, RequestType>{
//    private val result = MutableLiveData<Resource<ResultType>>()
//    private val supervisorJob = SupervisorJob()
//    suspend fun build() : NetworkBoundResource<ResultType, RequestType> {
//        withContext(Dispatchers.Main) {
//            result.value = Resource.loading(null)
//        }
//        CoroutineScope(coroutineContext).launch(supervisorJob){
//            try {
//                fetchFromNetwork()
//            } catch (e: Exception){
//                when(e){
//                    is NetworkErrorException, is UnknownHostException -> {
//                        setValue(Resource.error("Tidak ada koneksi internet", null))
//                    }
//                    else -> setValue(Resource.error(e.localizedMessage ?: "Maaf, terjadi kesalahan pada server", null))
//                }
//            }
//        }
//
//        return this
//    }
//
//    private suspend fun fetchFromNetwork() {
//        createCallAsync().apply {
//            setValue(Resource.loading(null))
//            if (isSuccessful) {
//                body()?.let {
//                    saveCallResult(it)
//                    setValue(Resource.success(processResponse(it)))
//                }
//            } else {
//                setValue(Resource.error(this.errorBody().toString(), null))
//            }
//        }
//    }
//
//    @MainThread
//    private fun setValue(newValue: Resource<ResultType>) {
//        if (result.value != newValue) result.postValue(newValue)
//    }
//
//    fun asLiveData() = result as LiveData<Resource<ResultType>>
//
//    @WorkerThread
//    protected abstract suspend fun processResponse(response: RequestType) : ResultType
//
//    @WorkerThread
//    protected abstract suspend fun saveCallResult(item: RequestType)
//
//    @MainThread
//    protected abstract suspend fun createCallAsync(): Response<RequestType>
}